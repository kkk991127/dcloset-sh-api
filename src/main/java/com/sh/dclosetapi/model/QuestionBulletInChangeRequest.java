package com.sh.dclosetapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class QuestionBulletInChangeRequest {

    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;

}
