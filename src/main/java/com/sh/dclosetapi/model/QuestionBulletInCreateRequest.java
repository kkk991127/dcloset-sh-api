package com.sh.dclosetapi.model;

import com.sh.dclosetapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class QuestionBulletInCreateRequest {

    private MemberGroup memberGroup;
    private LocalDate questionCreateDate;
    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;

}
