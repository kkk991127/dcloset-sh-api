package com.sh.dclosetapi.model;

import com.sh.dclosetapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class QuestionBulletInResponse {

    private Long id;
    private MemberGroup memberGroup;



}
