package com.sh.dclosetapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberCreateRequest {
    private String userId;
    private String memberPw;
    private String memberName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String memberAddress;
    private String memberDetailedAddress;
    private Integer postCode;
    private Boolean NYPersonalInfo;
    private Boolean NYMarketing;
}
