package com.sh.dclosetapi.entity;

import com.sh.dclosetapi.enums.MemberGroup;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class QuestionBulletIn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private MemberGroup memberGroup;

    @Column(nullable = false)
    private LocalDate questionCreateDate;

    @Column(nullable = false)
    private String questionTitle;

    @Column(nullable = false)
    private Integer questionPassword;

    @Column(columnDefinition = "Text")
    private String questionContent;


}
