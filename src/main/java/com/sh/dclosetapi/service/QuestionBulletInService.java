package com.sh.dclosetapi.service;

import com.sh.dclosetapi.entity.QuestionBulletIn;
import com.sh.dclosetapi.model.QuestionBulletInCreateRequest;
import com.sh.dclosetapi.model.QuestionBulletInItem;
import com.sh.dclosetapi.model.QuestionBulletInResponse;
import com.sh.dclosetapi.repository.QuestionBulletInRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionBulletInService {
    private final QuestionBulletInRepository questionBulletInRepository;
    public void setQuestionBulletIn(QuestionBulletInCreateRequest request){
        QuestionBulletIn addData = new QuestionBulletIn();
        addData.setMemberGroup(request.getMemberGroup());
        addData.setQuestionCreateDate(request.getQuestionCreateDate());
        addData.setQuestionTitle(request.getQuestionTitle());
        addData.setQuestionContent(request.getQuestionContent());
        addData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(addData);
    }

    public List<QuestionBulletInItem> getQuestionBulletIns(){
        List<QuestionBulletIn> originList = questionBulletInRepository.findAll();
        List<QuestionBulletInItem> result = new LinkedList<>();

        for(QuestionBulletIn questionBulletIn : originList){
            QuestionBulletInItem addItem = new QuestionBulletInItem();
            addItem.setId(addItem.getId());
            addItem.setMemberGroup(addItem.getMemberGroup());
            addItem.setQuestionCreateDate(addItem.getQuestionCreateDate());
            addItem.setQuestionTitle(addItem.getQuestionTitle());
            addItem.setQuestionContent(addItem.getQuestionContent());
            addItem.setQuestionPassword(addItem.getQuestionPassword());

            result.add(addItem);
        }
        return result;
    }

    public QuestionBulletInResponse getQuestionBulletIn(long id){
        QuestionBulletIn originData = questionBulletInRepository.findById(id).orElseThrow();

        QuestionBulletInResponse response = new QuestionBulletInResponse();
        response.setId(originData.getId());
        response.setMemberGroup(originData.getMemberGroup());

        return response;
    }
}
