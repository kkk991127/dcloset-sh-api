package com.sh.dclosetapi.controller;

import com.sh.dclosetapi.model.QuestionBulletInCreateRequest;
import com.sh.dclosetapi.model.QuestionBulletInItem;
import com.sh.dclosetapi.model.QuestionBulletInResponse;
import com.sh.dclosetapi.service.QuestionBulletInService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/questionBulletIn")
public class QuestionBulletInController {
    private final QuestionBulletInService questionBulletInService;

    @PutMapping("/new")
    public String setQuestionBulletIn(@RequestBody QuestionBulletInCreateRequest request){
        questionBulletInService.setQuestionBulletIn(request);
        return "ok";
    }
    @GetMapping("/all")
    public List<QuestionBulletInItem> getQuestionBulletIns(){return questionBulletInService.getQuestionBulletIns();}

    @GetMapping("/detail/{id}")
    public QuestionBulletInResponse getQuestionBulletIn(@PathVariable long id) {return questionBulletInService.getQuestionBulletIn(id);}
}
